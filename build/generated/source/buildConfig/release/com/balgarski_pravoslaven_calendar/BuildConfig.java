/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.balgarski_pravoslaven_calendar;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.balgarski_pravoslaven_calendar";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 39;
  public static final String VERSION_NAME = "3.9";
}
