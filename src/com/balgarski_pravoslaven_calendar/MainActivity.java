package com.balgarski_pravoslaven_calendar;

import com.balgarski_pravoslaven_calendar.DatiNaVelikden;
import com.balgarski_pravoslaven_calendar.Molitvi;
import com.balgarski_pravoslaven_calendar.Kalendar;
import com.balgarski_pravoslaven_calendar.Praznici;
import com.balgarski_pravoslaven_calendar.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    

    /** Called when the user clicks the button*/
    public void OpenKalendar(View view) {
        // Do something in response to button
    	Intent intent = new Intent(this, Kalendar.class);
    	startActivity(intent);
    } 
    
    /** Called when the user clicks the button */
    public void OpenPraznici(View view) {
        // Do something in response to button
    	Intent intent = new Intent(this, Praznici.class);
    	startActivity(intent);
    }
    
    /** Called when the user clicks the button */
    public void OpenMolitvi(View view) {
        // Do something in response to button
    	Intent intent = new Intent(this, Molitvi.class);
    	startActivity(intent);
    }
    
    /** Called when the user clicks the button */
    public void OpenDatiNaVelikden(View view) {
        // Do something in response to button
    	Intent intent = new Intent(this, DatiNaVelikden.class);
    	startActivity(intent);
    }
}
