package com.balgarski_pravoslaven_calendar;

import java.util.Calendar;

import com.balgarski_pravoslaven_calendar.R;

import android.os.Bundle;
import android.app.Activity;
//import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class Kalendar extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar rightNow = Calendar.getInstance();
        if(rightNow.get(Calendar.YEAR)==2020)
        {
            setContentView(R.layout.activity_pk_12_prev_year);
        }
        else
        {
            switch (rightNow.get(Calendar.MONTH)) {
                case Calendar.JANUARY:  setContentView(R.layout.activity_pk_01);
                break;
                case Calendar.FEBRUARY:  setContentView(R.layout.activity_pk_02);
                break;
                case Calendar.MARCH:  setContentView(R.layout.activity_pk_03);
                break;
                case Calendar.APRIL:  setContentView(R.layout.activity_pk_04);
                break;
                case Calendar.MAY:  setContentView(R.layout.activity_pk_05);
                break;
                case Calendar.JUNE:  setContentView(R.layout.activity_pk_06);
                break;
                case Calendar.JULY:  setContentView(R.layout.activity_pk_07);
                break;
                case Calendar.AUGUST:  setContentView(R.layout.activity_pk_08);
                break;
                case Calendar.SEPTEMBER:  setContentView(R.layout.activity_pk_09);
                break;
                case Calendar.OCTOBER:  setContentView(R.layout.activity_pk_10);
                break;
                case Calendar.NOVEMBER:  setContentView(R.layout.activity_pk_11);
                break;
                case Calendar.DECEMBER:  setContentView(R.layout.activity_pk_12);
                break;
            }
        }
    }

    /** Called when the user clicks the button */
    public void setContentView_12_prev_year(View view) {
    	setContentView(R.layout.activity_pk_12_prev_year);
    }

    public void setContentView_01(View view) {
    	setContentView(R.layout.activity_pk_01);    	
    }

    public void setContentView_02(View view) {
    	setContentView(R.layout.activity_pk_02);    	
    }

    public void setContentView_03(View view) {
    	setContentView(R.layout.activity_pk_03);    	
    }

    public void setContentView_04(View view) {
    	setContentView(R.layout.activity_pk_04);    	
    }

    public void setContentView_05(View view) {
    	setContentView(R.layout.activity_pk_05);    	
    }

    public void setContentView_06(View view) {
    	setContentView(R.layout.activity_pk_06);    	
    }

    public void setContentView_07(View view) {
    	setContentView(R.layout.activity_pk_07);    	
    }

    public void setContentView_08(View view) {
    	setContentView(R.layout.activity_pk_08);    	
    }

    public void setContentView_09(View view) {
    	setContentView(R.layout.activity_pk_09);    	
    }

    public void setContentView_10(View view) {
    	setContentView(R.layout.activity_pk_10);    	
    }

    public void setContentView_11(View view) {
    	setContentView(R.layout.activity_pk_11);    	
    }

    public void setContentView_12(View view) {
    	setContentView(R.layout.activity_pk_12);    	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pravoslaven_kalendar, menu);
        return true;
    }
}
