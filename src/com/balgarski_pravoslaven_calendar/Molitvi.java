package com.balgarski_pravoslaven_calendar;

import com.balgarski_pravoslaven_calendar.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class Molitvi extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_molitvi);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_molitvi, menu);
        return true;
    }
}
