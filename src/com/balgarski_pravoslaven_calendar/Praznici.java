package com.balgarski_pravoslaven_calendar;

import java.util.Calendar;
import com.balgarski_pravoslaven_calendar.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;

public class Praznici extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar rightNow = Calendar.getInstance();
        switch (rightNow.get(Calendar.MONTH)) {
	    	case Calendar.JANUARY:  setContentView(R.layout.activity_prz_01);
			break;
	    	case Calendar.FEBRUARY:  setContentView(R.layout.activity_prz_02);
			break;
	    	case Calendar.MARCH:  setContentView(R.layout.activity_prz_03);
			break;
	    	case Calendar.APRIL:  setContentView(R.layout.activity_prz_04);
			break;
	    	case Calendar.MAY:  setContentView(R.layout.activity_prz_05);
			break;
	    	case Calendar.JUNE:  setContentView(R.layout.activity_prz_06);
			break;
	    	case Calendar.JULY:  setContentView(R.layout.activity_prz_07);
			break;
	    	case Calendar.AUGUST:  setContentView(R.layout.activity_prz_08);
			break;
	    	case Calendar.SEPTEMBER:  setContentView(R.layout.activity_prz_09);
			break;
	    	case Calendar.OCTOBER:  setContentView(R.layout.activity_prz_10);
			break;
	    	case Calendar.NOVEMBER:  setContentView(R.layout.activity_prz_11);
			break;
	    	case Calendar.DECEMBER:  setContentView(R.layout.activity_prz_12);
			break;
        }

    }

    /*
    /** Called when the user clicks the button */
    public void setContentView_prz_01(View view) {
    	setContentView(R.layout.activity_prz_01);
    }

    public void setContentView_prz_02(View view) {
    	setContentView(R.layout.activity_prz_02);    	
    }

    public void setContentView_prz_03(View view) {
    	setContentView(R.layout.activity_prz_03);    	
    }

    public void setContentView_prz_04(View view) {
    	setContentView(R.layout.activity_prz_04);    	
    }

    public void setContentView_prz_05(View view) {
    	setContentView(R.layout.activity_prz_05);    	
    }

    public void setContentView_prz_06(View view) {
    	setContentView(R.layout.activity_prz_06);    	
    }

    public void setContentView_prz_07(View view) {
    	setContentView(R.layout.activity_prz_07);    	
    }

    public void setContentView_prz_08(View view) {
    	setContentView(R.layout.activity_prz_08);    	
    }

    public void setContentView_prz_09(View view) {
    	setContentView(R.layout.activity_prz_09);    	
    }

    public void setContentView_prz_10(View view) {
    	setContentView(R.layout.activity_prz_10);    	
    }

    public void setContentView_prz_11(View view) {
    	setContentView(R.layout.activity_prz_11);    	
    }

    public void setContentView_prz_12(View view) {
    	setContentView(R.layout.activity_prz_12);    	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_praznici, menu);
        return true;
    }
  }
