package com.balgarski_pravoslaven_calendar;

import com.balgarski_pravoslaven_calendar.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class DatiNaVelikden extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dati_na_velikden);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_dati_na_velikden, menu);
        return true;
    }
}
